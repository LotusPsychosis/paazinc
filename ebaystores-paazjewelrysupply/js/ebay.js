// Original design work, including graphics and all related scripts, Copyright (c) OCDesignsOnline.com, All Rights Reserved. Used with permission by Paaz-Jewelry-Imports.

jQuery(function($){
	//Right Click Protection
	//$(document).bind("contextmenu",function(e){ return false;});
	
	//Define seller ID
	var UserID = "paazjewelrysupply";
	var settings = {
		"UserID":UserID,
		"loadingClass": "loading",
		"serviceUrl": "http://ebay.sunandfuninoc.com/ebay.php"
	};
	
	//Content Area Mods
	if(pageName == "PageAboutMeViewStore") {
		$(".pagecontainer > table:eq(1) tr:first td:first").addClass("x-bg");
		$(".x-bg > table:eq(0) tr:eq(0) td:first").addClass("x-ctr");
		$(".x-bg > table:eq(1) tr:eq(1) td:first").addClass("x-ctr");
		$(".x-ctr > table:eq(0)").addClass("x-ctr");
	}else {
		$(".pagecontainer > table:eq(1) tr:first td:first").addClass("x-bg");
		$(".x-bg table:eq(1)").addClass("x-content");
		$(".gallery table:eq(0)").removeClass("x-content");
		$("#x-template table:eq(1)").removeClass("x-content");
		$(".x-content tr:first").addClass("x-hide");
		$("#x-template .x-content tr:first").removeClass("x-hide");
		$(".x-content").find("br[clear = none]").remove();
	}
	
	//Store header
	var header = "\n\r<div id=\"x-head\" style=\"display:none;\">" +
		"	<!-- head graphics -->" +
		"	<a id=\"x-head-logo\" href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\" title=\"Paaz-Jewelry-Imports eBay Store\"><!-- sp --><\/a>" +
		"	<!-- head search -->" +
		"	<form id=\"x-head-srch\" method=\"get\" name=\"search\" action=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\">" +
		"		 <input id=\"x-head-srch-bttn\" name=\"submit\" type=\"submit\" value=\"&nbsp\">" +
		"		 <input id=\"x-head-srch-sbox\" onblur=\"if (this.value == '') this.value = 'Store Search...';\" onfocus=\"if (this.value == 'Store Search...') this.value = '';\" value=\"Store Search...\" maxlength=\"300\" name=\"_nkw\"  type=\"text\">" +
		"	<\/form>" +
		"	<!-- head menu links -->" +
		"	<ul id=\"x-head-menu\">" +
		"		<li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\">Home<\/a><\/li>" +
		"		<li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/About-Us\">About Us<\/a><\/li>" +
		"		<li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Shipping-Information\">Shipping Information<\/a><\/li>" +
		"		<li><a href=\"http:\/\/contact.ebay.com\/ws\/eBayISAPI.dll?ReturnUserEmail&redirect=0&requested=paazjewelrysupply\">Contact Us<\/a><\/li>" +
		"<\/div>";
	if($(".x-bg").length > 0) { $("#x-head-pull").after(header); }
	
	//Store left column
	var left = "\n\r" +
		"	<!-- left categories menu -->" +	
		"	<div class=\"x-tbar x-tbar-cats\" title=\"Store Categories\"><!-- sp --><\/div>" +
		"	<div id=\"x-side-cats\" class=\"x-tmid\">" +
		"		 <ul class=\"lev1\">" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Beading-Supplies-\/_i.html?_custompageid=41102219&_fsub=861104219&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Beading Supplies<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Ear-Piercing-Supplies-\/_i.html?_custompageid=41102219&_fsub=726474419&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Ear Piercing Supplies<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Gemological-Instruments-\/_i.html?_custompageid=41102219&_fsub=861453219&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Gemological Instruments<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Glendo-GRS-\/_i.html?_custompageid=41102219&_fsub=507648719&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Glendo GRS<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Jewelry-\/_i.html?_custompageid=41102219&_fsub=1241351719&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Jewelry<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Jewelry-Gift-Boxes-Bags-\/_i.html?_custompageid=41102219&_fsub=860761719&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Jewelry Gift Boxes &amp; Bags<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Jewelry-Tags-Repair-Envelops-\/_i.html?_custompageid=41102219&_fsub=860780119&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Jewelry Tags &amp; Repair Envelops<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Jewelry-Tools-Supplies-\/_i.html?_custompageid=41102219&_fsub=860756519&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Jewelry Tools &amp; Supplies<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Lapidery-Supplies-\/_i.html?_custompageid=41102219&_fsub=870728019&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Lapidery Supplies<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Testering-Supplies-\/_i.html?_custompageid=41102219&_fsub=832395119&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Testering Supplies<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Watch-and-Clock-Supplies-\/_i.html?_custompageid=41102219&_fsub=725762619&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Watch and Clock Supplies<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Zip-Lock-Self-Adhesive-Bags-\/_i.html?_custompageid=41102219&_fsub=860764919&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Zip Lock &amp; Self Adhesive Bags<\/a><\/li>" +
		"		 	 <li><a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\/Other-\/_i.html?_custompageid=41102219&_fsub=1&_sid=1059228079&_storesessionid=87975860&_trksid=p4634.c0.m322\">Other<\/a><\/li>" +		
		"		 <\/ul>" +
		"	<\/div><div class=\"x-tbtm x-tbsh\"><!-- sp --><\/div>" +
		"	<!-- left newsletter text box -->" +
		"	<div class=\"x-tbar x-tbar-news\" title=\"Store Newsletter\"><!-- sp --><\/div>" + 
		"	<div id=\"x-side-news\" class=\"x-tmid\"><div class=\"x-tins\">" +
		"		 <p>Add my Store to your Favorites and receive my email newsletters about new items and special promotions!<\/p>" +
		"		 <p class=\"x-ckbx\"><input type=\"checkbox\" checked id=\"general\" name=\"nlchxbox\"><label for=\"general\">General Interest<\/label><\/p>" +
		"		 <a href=\"http:\/\/my.ebay.com\/ws\/eBayISAPI.dll?AcceptSavedSeller&amp;sellerid=paazjewelrysupply\" id=\"x-side-news-bttn\" title=\"Sign Up\"><!-- sign up --><\/a>" +
		"	<\/div><\/div><div class=\"x-tbtm\"><!-- sp --><\/div>" +
		"	<!-- left promo graphics -->" +
		"	<img src=\"http:\/\/tng1.arrowhitech.net\/ebaystores-paazjewelrysupply\/images\/x-side-whys.png\" class=\"x-prom\" width=\"210\" height=\"299\" alt=\"Why Shop With Us\">" +
		"	<img src=\"http:\/\/tng1.arrowhitech.net\/ebaystores-paazjewelrysupply\/images\/x-side-guar.png\" class=\"x-prom x-home\" width=\"210\" height=\"114\" alt=\"100% Satisfaction Guarantee\">" +
		"	<img src=\"http:\/\/tng1.arrowhitech.net\/ebaystores-paazjewelrysupply\/images\/x-side-hass.png\" class=\"x-prom x-home\" width=\"210\" height=\"110\" alt=\"Hassle Free Guarantee\">" +
		"	<a target=\"_blank\" href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\"><img src=\"http:\/\/www.sunandfuninoc.com\/sites\/jx\/w\/waymil\/ebay\/images\/x-side-stor.png\" class=\"x-prom x-temp\" width=\"210\" height=\"76\" alt=\"Visit Our eBay Store\"><\/a>" +
		"";
	if($(".x-content").length > 0) { $("#x-left-pull").after(left); }
	
	//Store categories population
	if($("#x-side-cats").length > 0) { if($("#LeftPanel .lcat").length > 0) { $("#x-side-cats").html($("#LeftPanel .lcat").html()); } }
	
	//Template categories population
	//if($("#x-template").length > 0) {$("#x-template #x-side-cats").loadCategories( $.extend({}, settings, { "service": {
	//	"callback": function( list ){ var strNewString = $('ul.lev1').html().replace(/\ebay.com/g,'ebay.com.au'); $('ul.lev1').html(strNewString); },
	//	"levels": 1,
	//	"orderBy" : "order"
	//} }) );}
	
	//Custom style additions
	if($("#x-head-menu").find("li").length > 0)					{ $("#x-head-menu").find("li:first").addClass("first"); }
	if($("#x-head-menu").find("li").length > 0)	  		  		{ $("#x-head-menu").find("li:last").addClass("last"); }
	if($("#LeftPanel ul.lev1").find("li").length > 0) 			{ $("#LeftPanel ul.lev1").find("li:first").addClass("first"); }
	if($("#LeftPanel ul.lev1").find("li").length > 0)	  		{ $("#LeftPanel ul.lev1").find("li:last").addClass("last"); }
	if($(".x-fp table.fixed").find("tr").length > 0)			{ $(".x-fp table.fixed").find("tr:first td:first").addClass("x-hide"); }
	if($("#x-feat .x-test table.fixed").find("tr").length > 0)	{ $("#x-feat .x-test table.fixed").find("tr:first td:first").removeClass("x-hide"); }
	if($("#x-main-tabs").length > 0)					  		{ $("#x-main-tabs").find("a:first img").addClass("first"); }
	if($("#x-main-tabs").length > 0)							{ $("#x-main-tabs").find("a:last img").addClass("last"); }
	if($("#x-sp").find("table").length > 0)						{ $("#x-sp").find("table tr:last").addClass("last"); }
	

	//Footer with links
	var footer = "\n\r<div id=\"x-foot-wrap\"><div id=\"x-foot\">" +
		"	<div id=\"x-foot-subm\">" +
		"		<a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\">Home<\/a> &nbsp;|&nbsp; " +
		"		<a href=\"http:\/\/feedback.ebay.com\/ws\/eBayISAPI.dll?ViewFeedback&userid=paazjewelrysupply\">Feedback<\/a> &nbsp;|&nbsp; " +
		"		<a href=\"http:\/\/contact.ebay.com\/ws\/eBayISAPI.dll?ReturnUserEmail&redirect=0&requested=paazjewelrysupply\">Contact Us<\/a>" +
		"	<\/div>" +
		"	<div id=\"x-foot-copy\">Copyright &copy; "+d.getFullYear()+" <a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\">Paaz Jewelry Imports<\/a>. All rights reserved. &nbsp;|&nbsp; <a href=\"http:\/\/stores.ebay.com\/Paaz-Jewelry-Imports\" target=\"_blank\">eBay Store Design<\/a><\/div>" +
		"<\/div><\/div>";
	if($(".x-content").length > 0) { $(".x-content").after(footer); }
	if($("#x-template").length > 0) { $("#x-foot").html($("#x-foot").html().replace("eBay Store Design","eBay Template Design")); }
	
	// Template featured items
	//$("#x-main-feat .x-tbar-feat").loadFeatured( $.extend({}, settings, { "service": {
	//
	// 	International sellers only, dollar sign currency
	//	"callback": function( list ){ var strNewString = $('#x-feat').html().replace(/\ebay.com/g,'ebay.com.au'); $('#x-feat').html(strNewString); },
	//	International sellers only, another currency symbol
	//	"callback": function( list ){ var strNewString = $('#x-feat').html().replace(/\ebay.com/g,'ebay.co.uk').replace(/\$/g,'&pound;').replace(/\/\&pound;/g,'$'); $('#x-feat').html(strNewString); },
	//
	//	"keyword": null,
	//	"category": null,
	//	"num": 4,
	//	"operator": "AND"
	//} }) );
	
});

	//Template terms tabs
	var tabs_cur_layer = "x-main-tb01";
	function tabs_swapLayers(id) { if (tabs_cur_layer) tabs_hideLayer(tabs_cur_layer); tabs_showLayer(id); tabs_cur_layer = id; }
	function tabs_showLayer(id) { var layer = tabs_getElemRefs(id); if (layer && layer.css) layer.css.display = "block"; }
	function tabs_hideLayer(id) { var layer = tabs_getElemRefs(id); if (layer && layer.css) layer.css.display = "none"; }
	function tabs_getElemRefs(id) { var el = (document.getElementById)? document.getElementById(id): (document.all)? document.all[id]: (document.layers)? document.layers[id]: null; if (el) el.css = (el.style)? el.style: el; return el; }
	window.addEventListener("load", function() {
		makeTabs("#x-main-tabs")
	});	
		
function makeTabs(selector) { 
    tab_lists_anchors = document.querySelectorAll(selector + " a"); 
    for (i = 0; i < tab_lists_anchors.length; i++) { 
        document.querySelectorAll(selector + " a")[i].addEventListener('click', function(e) { 
            for (i = 0; i < tab_lists_anchors.length; i++) {
                tab_lists_anchors[i].classList.remove("active");
            } 
            clicked_tab = e.target || e.srcElement; 
            clicked_tab.classList.add('active');  
        });
    } 
}